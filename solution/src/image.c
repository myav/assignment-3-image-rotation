#include <stdio.h>
#include <stdlib.h>

#include "../include/image.h"

struct image create_image(size_t image_width, size_t image_height) {
    struct image image = {
            .data = malloc(image_width * image_height * sizeof(struct pixel)),
            .width = image_width,
            .height = image_height
    };
    if (image.data == 0) {
        fprintf(stderr, "Can't create image\n");
        exit(-1);
    }
    return image;
}

struct pixel* get_pixel(struct image image, size_t x, size_t y) {
    size_t index = x + (image.width) * y;
    return image.data + index;
}

void free_image(struct image *image) {
    free(image->data);
}
