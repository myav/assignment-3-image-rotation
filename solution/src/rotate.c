#include "../include/rotate.h"
#include <stdio.h>

void rotate_pixel(struct image source, struct image rotated, size_t x, size_t y) {
    *get_pixel(rotated, source.height - y - 1, x) = *get_pixel(source, x, y);
}

struct image rotate(struct image const *source) {
    size_t width = source->width;
    size_t height = source->height;
    struct image rotated = create_image(height, width);
    for (size_t i = 0; i < width; i++) {
        for (size_t j = 0; j < height; j++) {
            rotate_pixel(*source, rotated, i, j);
        }
    }
    return rotated;
}
