#include <string.h>

#include "../include/bmp.h"

#define BMP_SIGNATURE 0x4d42
#define BMP_RESERVED 0
#define BMP_SIZE 40
#define BMP_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0
#define BMP_XPELS_PER_METER 0
#define BMP_YPELS_PER_METER 0
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


static size_t get_padding(size_t bmp_width) {
    if (bmp_width % 4 == 0) return 0;
    else return 4 - (bmp_width * 3) % 4;
}

static enum read_status check_header(struct bmp_header header) {
    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    } else if (header.biBitCount != BMP_BIT_COUNT) {
        return READ_INVALID_BITS;
    } else {
        return READ_OK;
    }
}

static enum read_status read_header(FILE *f, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, f) != 1) {
        return READ_INVALID_HEADER;
    }
    return check_header(*header);
}

static enum write_status write_header(FILE *f, struct bmp_header *header) {
    if (fwrite(header, sizeof(struct bmp_header), 1, f) != 1) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

static enum read_status read_pixel(FILE *f, struct pixel *pixels, size_t cnt) {
    if (fread(pixels, sizeof(struct pixel), cnt, f) != cnt) {
        return READ_ERROR;
    }
    return READ_OK;
}

static enum write_status write_pixel(FILE *f, struct pixel *pixels, size_t cnt) {
    if (fwrite(pixels, sizeof(struct pixel), cnt, f) != cnt) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

static struct bmp_header create_header(struct image const *img) {
    size_t padding = get_padding(img->width);
    const uint16_t file_size = img->height * (padding + img->width * 3) + sizeof(struct bmp_header);
    const uint16_t size_image = img->height * (img->width * sizeof(struct pixel));
    return (struct bmp_header) {.bfType = BMP_SIGNATURE, .bfileSize = file_size, .bfReserved = BMP_RESERVED, .bOffBits = sizeof(struct bmp_header), .biSize = BMP_SIZE, .biWidth = img->width, .biHeight = img->height, .biPlanes = BMP_PLANES, .biBitCount = BMP_BIT_COUNT, .biCompression = BMP_COMPRESSION, .biSizeImage = size_image, .biXPelsPerMeter = BMP_XPELS_PER_METER, .biYPelsPerMeter = BMP_YPELS_PER_METER, .biClrUsed = BMP_CLR_USED, .biClrImportant = BMP_CLR_IMPORTANT};
}

enum read_status from_bmp(FILE *in, struct image *image) {
    struct bmp_header header = {0};

    enum read_status header_status = read_header(in, &header);
    if (header_status != READ_OK) {
        return header_status;
    }

    if (fseek(in, header.bOffBits, SEEK_SET) != 0) {
        return READ_INVALID_SIGNATURE;
    }

    *image = create_image(header.biWidth, header.biHeight);

    struct pixel *pixels = image->data;
    size_t padding = get_padding(header.biWidth);
    size_t height = header.biHeight;
    size_t width = header.biWidth;

    for (size_t i = 0; i < height; i++, pixels += width) {
        padding = (i == height - 1) ? 0 : padding;

        enum read_status pixel_status = read_pixel(in, pixels, width);
        if (pixel_status != READ_OK) return pixel_status;

        if (fseek(in, (long) padding, SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        }

    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *image) {
    struct bmp_header header = create_header(image);

    enum write_status header_status = write_header(out, &header);
    if (header_status != WRITE_OK) {
        return header_status;
    }

    size_t padding = get_padding(image->width);
    struct pixel *pixels = image->data;
    size_t height = header.biHeight;
    size_t width = header.biWidth;

    for (size_t i = 0; i < height; i++, pixels += width) {
        padding = (i == height - 1) ? 0 : padding;

        enum write_status pixel_status = write_pixel(out, pixels, width);
        if (pixel_status != WRITE_OK) return pixel_status;

        if (fseek(out, (long) padding, SEEK_CUR) != 0) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

