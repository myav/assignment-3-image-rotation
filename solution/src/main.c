#include "../include/file_handler.h"
#include "../include/bmp.h"
#include "../include/rotate.h"
#include <stdio.h>

#define ARG_NUMBER_ERROR (-1)
#define INPUT_FILE_ERROR (-2)
#define OUTPUT_FILE_ERROR (-3)
#define FROM_BMP_ERROR (-4)
#define TO_BMP_ERROR (-5)

int main(int argc, char **argv) {

    if (argc != 3) {
        fprintf(stderr, "Not enough argc: %d\n. Input format: ./image-rotation input.bmp output.bmp", argc);
        return ARG_NUMBER_ERROR;
    }

    FILE *input_file;
    FILE *output_file;

    bool input_file_opened = open_file(&input_file, argv[1], "rb");
    if (!input_file_opened) {
        fprintf(stderr, "Can't open input file %d\n", input_file_opened);
        return INPUT_FILE_ERROR;
    }

    bool output_file_opened = open_file(&output_file, argv[2], "wb");
    if (!output_file_opened) {
        fprintf(stderr, "Can't open output file %d\n", output_file_opened);
        return OUTPUT_FILE_ERROR;
    }

    struct image source;

    enum read_status converted_from_bmp = from_bmp(input_file, &source);
    if (converted_from_bmp != READ_OK) {
        fprintf(stderr, "Can't open file as bmp");
        return FROM_BMP_ERROR;
    }
    close_file(&input_file);

    struct image result = rotate(&source);
    free_image(&source);

    enum write_status converted_to_bmp = to_bmp(output_file, &result);
    if (converted_to_bmp != WRITE_OK) {
        fprintf(stderr, "Can't save file as bmp");
        return TO_BMP_ERROR;
    }

    free_image(&result);
    close_file(&output_file);


    return 0;
}
