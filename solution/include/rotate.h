#ifndef UNTITLED_ROTATE_H
#define UNTITLED_ROTATE_H

#include "image.h"

typedef struct image (image_transformer) (struct image const* source);

image_transformer rotate;

#endif
