#ifndef UNTITLED_IMAGE_H
#define UNTITLED_IMAGE_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width, height;
    struct pixel* data;
};

struct image create_image(size_t image_width, size_t image_height);
struct pixel* get_pixel(struct image image, size_t x, size_t y);
void free_image(struct image *image);
#endif


